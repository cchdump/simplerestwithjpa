This is a simple REST application using Jersey and JPA.

It contains GET, POST, PUT and DELETE operations to add/update/remove CUSTOMER and ADDRESS information.