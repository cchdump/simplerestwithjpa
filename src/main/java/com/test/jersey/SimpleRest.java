package com.test.jersey;

import java.net.URI;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.jersey.exception.NotFoundException;
import com.test.jpa.DBHandler;
import com.test.jpa.model.Address;
import com.test.jpa.model.Customer;

@Stateless
@Path("")
public class SimpleRest {
    @EJB
    private DBHandler dbHandler;

    @Context
    UriInfo uriInfo;

    private static Logger LOGGER = LoggerFactory.getLogger(SimpleRest.class);

    @GET
    @Path("/customer/{username}/addresses")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getAddresses(@PathParam("username") String username) {
        LOGGER.debug("username:" + username);
        
        try {
            Customer customer = dbHandler.findByUsername(username);
            
            return Response.ok().entity(customer.getShippingAddresses().toArray(new Address[0])).build();
            
        } catch (NotFoundException e) {
            return Response.noContent().entity(e.getMessage()).status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            LOGGER.error("error", e);
            return Response.serverError().entity("internal service error").build();
        }
    }
    
    @GET
    @Path("/customer/{username}/address/{addressId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getAddress(@PathParam("username") String username, @PathParam("addressId") int addressId) {
        LOGGER.debug("username:" + username);
        
        try {
            Customer customer = dbHandler.findByUsername(username);
            List<Address> addresses = customer.getShippingAddresses();
            Address address = null;            
            
            for (Address tmp : addresses) {
                if (tmp.getId() == addressId) {
                    address = tmp;
                }
            }
            
            if (address != null) {
                return Response.ok().entity(address).build();
            } else {
                return Response.noContent().entity("address id: " + addressId 
                        + " not found for username: " + username).status(Response.Status.NOT_FOUND).build();
            }
        } catch (NotFoundException e) {
            return Response.noContent().entity(e.getMessage()).status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            LOGGER.error("error", e);
            return Response.serverError().entity("internal service error").build();
        }
    }
    
    @GET
    @Path("/customer/{username}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getCustomerByUsername(@PathParam("username") String username) {
        LOGGER.debug("username:" + username);
        try {
            Customer customer = dbHandler.findByUsername(username);

            LOGGER.debug("" + customer);

            return Response.ok(customer).build();
        } catch (NotFoundException e) {
            return Response.noContent().entity(e.getMessage()).status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            LOGGER.error("error", e);
            return Response.serverError().entity("internal service error").build();
        }
    }

    @GET
    @Path("/customerId/{customerId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getCustomerById(@PathParam("customerId") int customerId) {
        LOGGER.debug("customer id: " + customerId);

        try {
            Customer customer = dbHandler.findById(customerId);
            return Response.ok(customer).build();
        } catch (NotFoundException e) {
            return Response.noContent().entity(e.getMessage()).status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            LOGGER.error("error", e);
            return Response.serverError().entity("internal service error").build();
        }
    }

    @DELETE
    @Path("/customer/{username}")
    @Produces(MediaType.TEXT_HTML)
    public Response deleteCustomer(@PathParam("username") String username) {
        LOGGER.debug("username:" + username);
        try {
            dbHandler.delete(username);
            return Response.ok().build();
        } catch (NotFoundException e) {
            return Response.noContent().entity(e.getMessage()).status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            LOGGER.error("error", e);
            return Response.serverError().entity("internal service error").build();
        }
    }

    @POST
    @Path("/customer")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addCustomer(@FormParam("username") String username, @FormParam("firstName") String firstName,
            @FormParam("lastName") String lastName) {

        LOGGER.debug("username:{}, first:{}, last:{}", username, firstName, lastName);

        try {
            Customer customer = new Customer();
            customer.setUsername(username);
            customer.setFirst(firstName);
            customer.setLast(lastName);

            customer = dbHandler.addCustomer(customer);

            URI uri = uriInfo.getAbsolutePathBuilder().path("customer").path(String.valueOf(customer.getId())).build();
            Response response = Response.created(uri).build();

            return response;
        } catch (Exception e) {
            LOGGER.error("error", e);
            return Response.serverError().entity("internal service error").build();
        }
    }

    @PUT
    @Path("/customer/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updateCustomer(@PathParam("username") String username, @FormParam("firstName") String firstName,
            @FormParam("lastName") String lastName) {

        LOGGER.debug("username:{}, first:{}, last:{}", username, firstName, lastName);

        try {
            Customer customer = dbHandler.findByUsername(username);
            customer.setFirst(firstName);
            customer.setLast(lastName);

            dbHandler.update(customer);

            return Response.ok().build();
        } catch (NotFoundException e) {
            return addCustomer(username, firstName, lastName);
        } catch (Exception e) {
            LOGGER.error("error", e);
            return Response.serverError().entity("internal service error").build();
        }
    }

    @POST
    @Path("/customer/{username}/address")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addAddress(@PathParam("username") String username, @FormParam("street") String street,
            @FormParam("city") String city, @FormParam("state") String state, @FormParam("country") String country,
            @FormParam("zip") int zip) {

        LOGGER.debug("username:{}, street:{}, city:{}, state:{}, country:{}, zip:{}", username, street, city, state,
                country, zip);

        try {
            Address address = dbHandler.addAddress(street, city, state, country, zip, username);

            URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(address.getId())).build();

            Response response = Response.created(uri).build();

            return response;
        } catch (Exception e) {
            LOGGER.error("error", e);
            return Response.serverError().entity("internal service error").build();
        }
    }

    @PUT
    @Path("/customer/{username}/address/{addressId}")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updateAddress(@PathParam("username") String username, @FormParam("street") String street,
            @FormParam("city") String city, @FormParam("state") String state, @FormParam("country") String country,
            @FormParam("zip") int zip, @PathParam("addressId") int addressId) {

        LOGGER.debug("username:{}, street:{}, city:{}, state:{}, country:{}, zip:{}, addressId:{}", username, street,
                city, state, country, zip, addressId);

        try {
            Address address = dbHandler.updateAddress(street, city, state, country, zip, username, addressId);

            if (address != null) {
                return Response.ok().build();
            } else {
                return Response.noContent().entity("address id:" + addressId).status(Status.NOT_FOUND).build();
            }
        } catch (Exception e) {
            LOGGER.error("error", e);
            return Response.serverError().entity("internal service error").build();
        }
    }

    @DELETE
    @Path("/customer/{username}/address/{addressId}")
    @Produces(MediaType.TEXT_HTML)
    public Response deleteAddress(@PathParam("username") String username, @PathParam("addressId") int addressId) {
        LOGGER.debug("username:" + username + ", addressId:" + addressId);
        try {
            dbHandler.deleteAddress(username, addressId);
            return Response.ok().build();
        } catch (NotFoundException e) {
            return Response.noContent().entity(e.getMessage()).status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            LOGGER.error("error", e);
            return Response.serverError().entity("internal service error").build();
        }
    }
    
    @GET
    @Path("/time")
    @Produces({ MediaType.APPLICATION_XML })
    public XmlResponse whatTimeIsIt() {
        Date date = new Date();
        XmlResponse response = new XmlResponse();
        response.setMessage("time: " + date);

        return response;
    }

    @GET
    @Path("/time")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public String whatTimeIsItInJson() {
        Date date = new Date();
        return "time: " + date;
    }
}
