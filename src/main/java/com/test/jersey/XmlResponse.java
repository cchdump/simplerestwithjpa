package com.test.jersey;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class XmlResponse {
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
