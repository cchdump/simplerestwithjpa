package com.test.jpa;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.eclipse.persistence.jpa.JpaHelper;
import org.eclipse.persistence.queries.QueryByExamplePolicy;
import org.eclipse.persistence.queries.ReadObjectQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.jersey.exception.NotFoundException;
import com.test.jpa.model.Address;
import com.test.jpa.model.Customer;

@Stateless
public class DBHandler {    
    private static Logger LOGGER = LoggerFactory.getLogger(DBHandler.class);
    
    @PersistenceUnit(unitName = "simpleJPA")
    private EntityManagerFactory emf;
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public Customer addCustomer(Customer customer) {
        EntityManager em = getEntityManager();
        em.persist(customer);
        em.flush(); // to be able to obtain id right away.
        return customer;
    }
    
    public Address addAddress(String street, String city, String state, String country, int zip, 
            String username) {
        
        EntityManager em = getEntityManager();

        Customer example = new Customer();
        example.setUsername(username);

        QueryByExamplePolicy policy = new QueryByExamplePolicy();
        policy.excludeDefaultPrimitiveValues();
        ReadObjectQuery readQuery = new ReadObjectQuery(example, policy);
        Query query = JpaHelper.createQuery(readQuery, em);

        Customer customer = (Customer) query.getSingleResult();

        Address address = new Address();
        address.setStreet(street);
        address.setCity(city);
        address.setState(state);
        address.setCountry(country);
        address.setZip(zip);
        address.setOwner(customer);

        customer.addAddress(address);

        em.persist(address);
        
        em.flush();
                
        return address;
    }


    public Address updateAddress(String street, String city, String state, String country, int zip, 
            String username, int addressId) {
        
        EntityManager em = getEntityManager();

        Customer example = new Customer();
        example.setUsername(username);

        QueryByExamplePolicy policy = new QueryByExamplePolicy();
        policy.excludeDefaultPrimitiveValues();
        ReadObjectQuery readQuery = new ReadObjectQuery(example, policy);
        Query query = JpaHelper.createQuery(readQuery, em);

        Customer customer = (Customer) query.getSingleResult();

        Address address = null;

        for (Address tmp : customer.getShippingAddresses()) {
            if (tmp.getId() == addressId) {
                address = tmp;
                break;
            }
        }
        
        if (address != null) {
            address.setStreet(street);
            address.setCity(city);
            address.setState(state);
            address.setCountry(country);
            address.setZip(zip);
    
            em.persist(address);
            
            em.flush();
        }
                
        return address;
    }

    
    public Customer findByUsername(String username) throws NotFoundException {
        try {
            Customer example = new Customer();
            example.setUsername(username);

            QueryByExamplePolicy policy = new QueryByExamplePolicy();
            policy.excludeDefaultPrimitiveValues();
            ReadObjectQuery readQuery = new ReadObjectQuery(example, policy);
            Query query = JpaHelper.createQuery(readQuery, getEntityManager());

            Customer customer = (Customer) query.getSingleResult();

            return customer;
        } catch (NoResultException e) {
            LOGGER.warn("username: " + username + " not found");
            throw new NotFoundException("username: " + username + " not found");
        }
    }

    public void delete(String username) throws NotFoundException {
        try {
            EntityManager em = getEntityManager();
            Customer example = new Customer();
            example.setUsername(username);

            QueryByExamplePolicy policy = new QueryByExamplePolicy();
            policy.excludeDefaultPrimitiveValues();
            ReadObjectQuery readQuery = new ReadObjectQuery(example, policy);
            Query query = JpaHelper.createQuery(readQuery, em);

            Customer customer = (Customer) query.getSingleResult();
            em.flush();
            em.remove(customer);
        } catch (NoResultException e) {
            LOGGER.warn("username: " + username + " not found");
            throw new NotFoundException("username: " + username + " not found");
        }
    }
    
    public void deleteAddress(String username, int addressId) throws NotFoundException {
        try {
            EntityManager em = getEntityManager();
            Customer example = new Customer();
            example.setUsername(username);

            QueryByExamplePolicy policy = new QueryByExamplePolicy();
            policy.excludeDefaultPrimitiveValues();
            ReadObjectQuery readQuery = new ReadObjectQuery(example, policy);
            Query query = JpaHelper.createQuery(readQuery, em);

            Customer customer = (Customer) query.getSingleResult();
            Address address = null;
            
            for (Address tmp : customer.getShippingAddresses()) {
                if (tmp.getId() == addressId) {
                    address = tmp;
                    break;
                }
            }
            
            if (address != null) {
                LOGGER.debug("remove address with id:" + address.getId());
                em.remove(address);
                customer.removeAddress(address);
                em.flush();
            }            
            
            
        } catch (NoResultException e) {
            LOGGER.warn("username: " + username + " not found");
            throw new NotFoundException("username: " + username + " not found");
        }        
    }

    public void update(Customer customer) {
        EntityManager em = getEntityManager();
        em.merge(customer);
    }

    public Customer findById(int customerId) throws NotFoundException {
        Customer customer = null;

        EntityManager em = getEntityManager();
        customer = em.find(Customer.class, customerId);

        if (customer == null) {
            throw new NotFoundException("customer id: " + customerId + " not found");
        }

        return customer;
    }
}
