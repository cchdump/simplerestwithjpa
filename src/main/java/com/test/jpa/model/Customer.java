package com.test.jpa.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the customer database table.
 * 
 */
@Entity
@Table(name="customer")
@XmlRootElement
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String first;

	private String last;
	
	private String username;
	
	@OneToMany(mappedBy="owner", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private List<Address> shippingAddresses = new LinkedList<Address>();

    public Customer() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirst() {
		return this.first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getLast() {
		return this.last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
		String str = "customer: id:" + id + ", username:" + this.username 
		    + ", firstName:" + this.first + ", lastName:" + this.last;

		for (Address address : shippingAddresses) {
		    str += "\n" + address;
		}
    
		return str;
    }

    public void addAddress(Address address) {
        if (shippingAddresses.contains(address)) {
            return;
        }
        
        shippingAddresses.add(address);
        address.setOwner(this);
    }
    
    public List<Address> getShippingAddresses() {
        return shippingAddresses;
    }
    
    public void setShippingAddresses(List<Address> addresses) {
        for (Address address : addresses) {
            addAddress(address);
        }
    }
    
    public void removeAddress(Address address) {
        if (!shippingAddresses.contains(address)) {
            return;
        }
        
        shippingAddresses.remove(address);
    }    
}